package aula.fib.br.urnaeletronica

import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import android.media.MediaPlayer



class UrnaEletronica : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_urna_eletronica)

        var campoCandidato = findViewById<TextView>(R.id.campoCandidato)
        var btNum0 = findViewById<Button>(R.id.btNum0)
        var btNum1 = findViewById<Button>(R.id.btNum1)
        var btNum2 = findViewById<Button>(R.id.btNum2)
        var btNum3 = findViewById<Button>(R.id.btNum3)
        var btNum4 = findViewById<Button>(R.id.btNum4)
        var btNum5 = findViewById<Button>(R.id.btNum5)
        var btNum6 = findViewById<Button>(R.id.btNum6)
        var btNum7 = findViewById<Button>(R.id.btNum7)
        var btNum8 = findViewById<Button>(R.id.btNum8)
        var btNum9 = findViewById<Button>(R.id.btNum9)
        var btBranco = findViewById<Button>(R.id.btBranco)
        var btCorrige = findViewById<Button>(R.id.btCorrige)
        var btConfirma = findViewById<Button>(R.id.btConfirma)

        btNum0.setOnClickListener {
            campoCandidato.text = campoCandidato.text.toString() + "0";
        }
        btNum1.setOnClickListener {
            campoCandidato.text = campoCandidato.text.toString() + "1";
        }
        btNum2.setOnClickListener {
            campoCandidato.text = campoCandidato.text.toString() + "2";
        }
        btNum3.setOnClickListener {
            campoCandidato.text = campoCandidato.text.toString() + "3";
        }
        btNum4.setOnClickListener {
            campoCandidato.text = campoCandidato.text.toString() + "4";
        }
        btNum5.setOnClickListener {
            campoCandidato.text = campoCandidato.text.toString() + "5";
        }
        btNum6.setOnClickListener {
            campoCandidato.text = campoCandidato.text.toString() + "6";
        }
        btNum7.setOnClickListener {
            campoCandidato.text = campoCandidato.text.toString() + "7";
        }
        btNum8.setOnClickListener {
            campoCandidato.text = campoCandidato.text.toString() + "8";
        }
        btNum9.setOnClickListener {
            campoCandidato.text = campoCandidato.text.toString() + "9";
        }

        btCorrige.setOnClickListener {
            campoCandidato.text = "";
        }

        btBranco.setOnClickListener{
            campoCandidato.text = "";
            Toast.makeText(this@UrnaEletronica, "Voto em branco realizado com sucesso!!", Toast.LENGTH_LONG).show();
        }

        btConfirma.setOnClickListener{
            val intent = Intent(this@UrnaEletronica, CandidatoVotado::class.java)
            intent.putExtra("candidato", campoCandidato.text.toString())
            startActivity(intent)
        }
    }
}

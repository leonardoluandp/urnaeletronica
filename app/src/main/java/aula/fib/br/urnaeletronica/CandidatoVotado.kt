package aula.fib.br.urnaeletronica

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.Toast
import android.media.MediaPlayer
import android.widget.Button
import android.widget.TextView


class CandidatoVotado : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_candidato_votado)

        val intent = intent
        if (intent != null) {
            val candidato = intent.getStringExtra("candidato")
            var imagemCandidato = findViewById<ImageView>(R.id.imagemCandidato)
            var txtCandidato = findViewById<TextView>(R.id.txtCandidato)
            var btConfirma = findViewById<Button>(R.id.btConfirma)

            if(candidato == "10"){
                imagemCandidato.setImageResource(R.mipmap.naruto);
                txtCandidato.text = "Naruto"
            }else if(candidato == "11"){
                imagemCandidato.setImageResource(R.mipmap.sasuke);
                txtCandidato.text = "Sasuke"
            }else if(candidato == "12"){
                imagemCandidato.setImageResource(R.mipmap.sasuke);
                txtCandidato.text = "Sasuke Universo 2"
            }else if(candidato == "13"){
                imagemCandidato.setImageResource(R.mipmap.sakura);
                txtCandidato.text = "Sakura"
            }else if(candidato == "14"){
                imagemCandidato.setImageResource(R.mipmap.kakashi);
                txtCandidato.text = "Kakashi"
            }

            btConfirma.setOnClickListener {
                Toast.makeText(this@CandidatoVotado, "Candidato votado com sucesso!!", Toast.LENGTH_LONG).show();
                val intent = Intent(this@CandidatoVotado, UrnaEletronica::class.java)
                startActivity(intent)
            }
        }
    }
}
